package com.example.demomybatis.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseMessageDelete {


    private String messageDelete;

    public String getMessageDelete() {
        return messageDelete;
    }

    public void setMessageDelete(String messageDelete) {
        this.messageDelete = messageDelete;
    }

    public Boolean getSuccessDelete() {
        return successDelete;
    }

    public void setSuccessDelete(Boolean successDelete) {
        this.successDelete = successDelete;
    }

    private Boolean successDelete;
}
