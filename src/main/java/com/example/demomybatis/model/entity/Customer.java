package com.example.demomybatis.model.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer customerID;
    private String customerName;
    private String customerAdd;
    private String customerPh;

}
