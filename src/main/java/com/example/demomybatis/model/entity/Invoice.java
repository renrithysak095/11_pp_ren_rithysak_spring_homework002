package com.example.demomybatis.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer invoiceID;
    @Builder.Default
    private LocalDateTime invoiceDate = LocalDateTime.now();
    private Customer customer;
    private List<Product> productList;


}
