package com.example.demomybatis.service.ServiceMain;

import com.example.demomybatis.model.entity.Invoice;
import com.example.demomybatis.model.request.InvoiceRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface InvoiceService {

    List<Invoice> getAll();

    Integer insertData(InvoiceRequest invoiceRequest);


    Invoice getInvoiceById(Integer id);

    Integer deleteByID(Integer id);

    Integer UpdateByID(Integer id, InvoiceRequest invoiceRequest);
}
