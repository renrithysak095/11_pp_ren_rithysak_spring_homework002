package com.example.demomybatis.service.ServiceMain;
import com.example.demomybatis.model.entity.Product;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {

    List<Product> getAll();

    Product getByID(Integer id);

    Product insertData(Product product);

    Product UpdateData(Integer id, Product product);

    Product deleteData(Integer id);
}
