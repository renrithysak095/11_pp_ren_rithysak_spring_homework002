package com.example.demomybatis.service.ServiceMain;

import com.example.demomybatis.model.entity.Customer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CustomerService {
    List<Customer> getAll();

    Customer getByID(Integer id);

    Customer insertData(Customer customer);

    Customer UpdateData(Integer id, Customer customer);

    Customer deleteData(Integer id);
}
