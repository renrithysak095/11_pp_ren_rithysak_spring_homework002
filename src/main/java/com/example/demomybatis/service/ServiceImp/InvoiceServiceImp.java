package com.example.demomybatis.service.ServiceImp;

import com.example.demomybatis.model.entity.Invoice;
import com.example.demomybatis.model.request.InvoiceRequest;
import com.example.demomybatis.repository.InvoiceRepository;
import com.example.demomybatis.service.ServiceMain.InvoiceService;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Component
@Repository
public class InvoiceServiceImp implements InvoiceService {

    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAll() {
        return invoiceRepository.getAll();
    }

    @Override
    public Integer insertData(InvoiceRequest invoiceRequest) {
        Integer in = invoiceRepository.insertData(invoiceRequest);
        for(int i = 0; i<invoiceRequest.getProductList().size();i++) {
            invoiceRepository.insertInvoiceDetail(in, invoiceRequest.getProductList().get(i));
        }
        return in;
    }

    @Override
    public Invoice getInvoiceById(Integer id) {
        return invoiceRepository.getInvoiceById(id);
    }

    @Override
    public Integer deleteByID(Integer id) {
        return invoiceRepository.deleteByID(id);
    }

    @Override
    public Integer UpdateByID(Integer id, InvoiceRequest invoiceRequest) {
        Integer in = invoiceRepository.updateByID(id, invoiceRequest);
        if(in == 1){
            invoiceRepository.deleteInvoiceDetail(id);
            for(int i = 0; i<invoiceRequest.getProductList().size();i++) {
                invoiceRepository.insertInvoiceDetail(id, invoiceRequest.getProductList().get(i));
            }
        }
        return in;
    }
}
