package com.example.demomybatis.service.ServiceImp;

import com.example.demomybatis.model.entity.Product;
import com.example.demomybatis.repository.ProductRepository;
import com.example.demomybatis.service.ServiceMain.ProductService;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Component
@Repository
public class ProductServiceImp implements ProductService {
    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAll() {
        return productRepository.getAll();
    }
    @Override
    public Product getByID(Integer id) {
        return productRepository.getByID(id);
    }
    @Override
    public Product insertData(Product product) {
        return productRepository.insertData(product);
    }
    @Override
    public Product UpdateData(Integer id,Product product) {
        return productRepository.UpdateData(id,product);
    }
    @Override
    public Product deleteData(Integer id) {
        return productRepository.deleteData(id);
    }
}
