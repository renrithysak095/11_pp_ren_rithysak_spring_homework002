package com.example.demomybatis.service.ServiceImp;

import com.example.demomybatis.model.entity.Customer;
import com.example.demomybatis.repository.CustomerRepository;
import com.example.demomybatis.service.ServiceMain.CustomerService;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;
@Component
@Repository
public class CustomerServiceImp implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAll() {
        return customerRepository.getAll();
    }

    @Override
    public Customer getByID(Integer id) {
        return customerRepository.getByID(id);
    }

    @Override
    public Customer insertData(Customer customer) {
        return customerRepository.insertData(customer);
    }

    @Override
    public Customer UpdateData(Integer id, Customer customer) {
        return customerRepository.UpdateData(id,customer);
    }

    @Override
    public Customer deleteData(Integer id) {
        return customerRepository.deleteData(id);
    }
}
