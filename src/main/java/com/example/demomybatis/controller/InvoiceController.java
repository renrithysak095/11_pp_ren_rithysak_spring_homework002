package com.example.demomybatis.controller;

import com.example.demomybatis.model.entity.Customer;
import com.example.demomybatis.model.entity.Invoice;
import com.example.demomybatis.model.entity.Product;
import com.example.demomybatis.model.request.InvoiceRequest;
import com.example.demomybatis.model.response.ResponseMessage;
import com.example.demomybatis.model.response.ResponseMessageDelete;
import com.example.demomybatis.service.ServiceMain.InvoiceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/inovice/api/v2")
public class InvoiceController {

    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    //ALl Data
    @GetMapping("/get-all-invoice")
    public ResponseEntity<?> getAll() {
        ResponseMessage responseMessage = new ResponseMessage<ArrayList<Customer>>();
        responseMessage.setMessage("Invoice Fetched All Successfully ");
        responseMessage.setSuccess(true);
        responseMessage.setPayload(invoiceService.getAll());
        return ResponseEntity.ok().body(responseMessage);
    }

    //Insert Data
    @PostMapping("/add-new-invoice")
    public ResponseEntity<?> insertData(@RequestBody InvoiceRequest invoiceRequest) {
        Integer in = invoiceService.insertData(invoiceRequest);
        ResponseMessage responseMessage = new ResponseMessage<Invoice>();
        responseMessage.setMessage("Add new Invioce is Successfully");
        responseMessage.setSuccess(true);
        responseMessage.setPayload(invoiceService.getInvoiceById(in));
        return ResponseEntity.ok().body(responseMessage);
    }

    //Getting by ID
    @GetMapping("/add-invoice-by-id/{id}")
    public ResponseEntity<?> getById(@PathVariable Integer id) {
        ResponseMessage responseMessage = new ResponseMessage<Invoice>();
        responseMessage.setMessage("Invoice Get By ID Successfully");
        responseMessage.setSuccess(true);
        responseMessage.setPayload(invoiceService.getInvoiceById(id));
        if (invoiceService.getInvoiceById(id) == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(responseMessage);
    }

    //Delete by ID
    @DeleteMapping("/delete-invoice-by-id/{id}")
    public ResponseEntity<?> deleteByID(@PathVariable Integer id) {
        Integer in = invoiceService.deleteByID(id);
        ResponseMessageDelete responseMessageDelete = new ResponseMessageDelete();
        responseMessageDelete.setMessageDelete("Invoice Delete By ID Successfully");
        responseMessageDelete.setSuccessDelete(true);
        if (in == -1) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(responseMessageDelete);
    }

    //Update By ID
    @PutMapping("/update-invoice-by-id/{id}")
    public ResponseEntity<?> UpdateByID(@PathVariable Integer id, @RequestBody InvoiceRequest invoiceRequest) {
        ResponseMessage responseMessage = new ResponseMessage<Invoice>();
        Integer in = invoiceService.UpdateByID(id, invoiceRequest);
        if (in == 1) {
            responseMessage.setMessage("Invoice Update Successfully");
            responseMessage.setSuccess(true);
            responseMessage.setPayload(invoiceService.getInvoiceById(id));
            return ResponseEntity.ok().body(responseMessage);
        }
        return ResponseEntity.notFound().build();
    }
}
