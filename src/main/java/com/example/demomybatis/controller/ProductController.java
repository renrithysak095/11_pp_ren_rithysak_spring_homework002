package com.example.demomybatis.controller;

import com.example.demomybatis.model.entity.Product;
import com.example.demomybatis.model.response.ResponseMessage;
import com.example.demomybatis.model.response.ResponseMessageDelete;
import com.example.demomybatis.service.ServiceMain.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@RestController
@RequestMapping("/product/api/v1")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }
    //ALl Data
    @GetMapping("/get-all-product")
    public ResponseEntity<?> getAllData() {
        ResponseMessage responseMessage = new ResponseMessage<ArrayList<Product>>();
        responseMessage.setMessage("Product Fetched All Successfully ");
        responseMessage.setSuccess(true);
        responseMessage.setPayload(productService.getAll());
        return ResponseEntity.ok().body(responseMessage);
    }
    //Data By ID
    @GetMapping("/get-product-by-id/{id}")
    public ResponseEntity<?> getByID(@PathVariable Integer id) {
        ResponseMessage responseMessage = new ResponseMessage<Product>();
        if (productService.getByID(id) == null) {
            return ResponseEntity.notFound().build();
        }
        responseMessage.setPayload(productService.getByID(id));
        responseMessage.setMessage("Product Fetched Successfully");
        responseMessage.setSuccess(true);
        return ResponseEntity.ok().body(responseMessage);
    }

    //Insert Data
    @PostMapping("/add-new-product")
    public ResponseEntity<?> insertData(@RequestBody Product product) {
        ResponseMessage responseMessage = new ResponseMessage<ArrayList<Product>>();
        responseMessage.setMessage("Product Has Add Successfully");
        responseMessage.setSuccess(true);
        responseMessage.setPayload(productService.insertData(product));
        return ResponseEntity.ok().body(responseMessage);
    }
    //Update Product By ID
    @PutMapping("/update-product-by-id/{id}")
    @ResponseBody
    public ResponseEntity<ResponseMessage<Product>> UpdateID(@PathVariable Integer id,
                                                             @RequestBody Product product) {
        ResponseMessage responseMassage = new ResponseMessage<ArrayList<Product>>();
        if (productService.getByID(id) == null) {
            return ResponseEntity.notFound().build();
        }
        responseMassage.setPayload(productService.UpdateData(id, product));
        responseMassage.setMessage("Product Updated Successfully");
        responseMassage.setSuccess(true);
        return ResponseEntity.ok().body(responseMassage);
    }

    //Delete Product By ID
    @DeleteMapping("/delete-product-by-id/{id}")
    public ResponseEntity<ResponseMessageDelete> deleteID(@PathVariable Integer id) {
        ResponseMessageDelete responseMessageDelete = new ResponseMessageDelete();
        ResponseMessage responseMessage = new ResponseMessage<ArrayList<Product>>();
        if (productService.getByID(id) == null) {
            return ResponseEntity.notFound().build();
        }
        responseMessage.setPayload(productService.deleteData(id));
        responseMessageDelete.setMessageDelete("Product Delete Successfully");
        responseMessageDelete.setSuccessDelete(true);
        return ResponseEntity.ok().body(responseMessageDelete);
    }
}
