package com.example.demomybatis.controller;
import com.example.demomybatis.model.entity.Customer;
import com.example.demomybatis.model.entity.Product;
import com.example.demomybatis.model.response.ResponseMessage;
import com.example.demomybatis.model.response.ResponseMessageDelete;
import com.example.demomybatis.service.ServiceMain.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/customer/api/v2")
public class CustomerController {
    private final CustomerService customerService;
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    //ALl Data
    @GetMapping("/get-all-customer")
    public ResponseEntity<?> getAllData() {
        ResponseMessage responseMessage = new ResponseMessage<ArrayList<Customer>>();
        responseMessage.setMessage("Customer Fetched All Successfully ");
        responseMessage.setSuccess(true);
        responseMessage.setPayload(customerService.getAll());
        return ResponseEntity.ok().body(responseMessage);
    }

    //Data By ID
    @GetMapping("/get-customer-by-id/{id}")
    public ResponseEntity<?> getByID(@PathVariable Integer id) {
        ResponseMessage responseMessage = new ResponseMessage<Customer>();
        if (customerService.getByID(id) == null) {
            return ResponseEntity.notFound().build();
        }
        responseMessage.setPayload(customerService.getByID(id));
        responseMessage.setMessage("Customer Fetched Successfully");
        responseMessage.setSuccess(true);
        return ResponseEntity.ok().body(responseMessage);
    }

    //Insert Data
    @PostMapping("/add-new-customer")
    public ResponseEntity<?> insertData(@RequestBody Customer customer) {
        ResponseMessage responseMessage = new ResponseMessage<ArrayList<Customer>>();
        responseMessage.setMessage("Customer Has Add Successfully");
        responseMessage.setSuccess(true);
        responseMessage.setPayload(customerService.insertData(customer));
        return ResponseEntity.ok().body(responseMessage);
    }

    //Update Customer By ID
    @PutMapping("/update-customer-by-id/{id}")
    @ResponseBody
    public ResponseEntity<ResponseMessage<Product>> UpdateID(@PathVariable Integer id,
                                                             @RequestBody Customer customer) {
        ResponseMessage responseMassage = new ResponseMessage<ArrayList<Customer>>();
        if (customerService.getByID(id) == null) {
            return ResponseEntity.notFound().build();
        }
        responseMassage.setPayload(customerService.UpdateData(id, customer));
        responseMassage.setMessage("Customer Updated Successfully");
        responseMassage.setSuccess(true);
        return ResponseEntity.ok().body(responseMassage);
    }

    //Delete Customer By ID
    @DeleteMapping("/delete-customer-by-id/{id}")
    public ResponseEntity<ResponseMessageDelete> deleteID(@PathVariable Integer id) {
        ResponseMessageDelete responseMessageDelete = new ResponseMessageDelete();
        ResponseMessage responseMessage = new ResponseMessage<ArrayList<Customer>>();
        if (customerService.getByID(id) == null) {
            return ResponseEntity.notFound().build();
        }
        responseMessage.setPayload(customerService.deleteData(id));
        responseMessageDelete.setMessageDelete("Customer Delete Successfully");
        responseMessageDelete.setSuccessDelete(true);
        return ResponseEntity.ok().body(responseMessageDelete);

    }
}

