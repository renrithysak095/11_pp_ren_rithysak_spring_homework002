package com.example.demomybatis.repository;

import com.example.demomybatis.model.entity.Product;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
@Repository
public interface ProductRepository {

    //Select All
    @Select("""
            SELECT * FROM product order by product_id asc;
            """)
    @Results(
            id = "productMap",value = {
            @Result(property = "productID", column = "product_id"),
            @Result(property = "productName", column = "product_name"),
            @Result(property = "productPrice", column = "product_price")
    }
    )
    List <Product> getAll();

    //Select By ID
    @Select("""
            SELECT * FROM product WHERE product_id = #{id};
            """)
    @ResultMap("productMap")
    Product getByID(Integer id);

    //Insert Into Product
    @Select("""
            INSERT into product(product_name, product_price) VALUES (#{product.productName},#{product.productPrice})
            RETURNING * 
            """)
    @ResultMap("productMap")
    Product insertData(@Param("product") Product product);


    //Update Data
    @Select("""
            UPDATE product SET product_name = #{product.productName} , product_price = #{product.productPrice}
            WHERE product_id = #{id}
                RETURNING *
            """)
    @ResultMap("productMap")
    Product UpdateData(@PathVariable Integer id,@Param("product") Product product);

    @Select("""
            DELETE from product where product_id = #{id}
            """)
    Product deleteData(Integer id);

    @Select("""
            Select product_name, product_price, pt.product_id
            from product pt inner join invoice_detail id on pt.product_id = id.product_id
            where invoice_id = #{invoiceID};
            """)

    @ResultMap("productMap")
    List<Product> getAllProductByInvoiceID(Integer invoiceID);


}
