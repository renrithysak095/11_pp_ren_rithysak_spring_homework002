package com.example.demomybatis.repository;

import com.example.demomybatis.model.entity.Invoice;
import com.example.demomybatis.model.request.InvoiceRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface InvoiceRepository {


    @Insert("insert into invoice_detail (invoice_id, product_id) values(#{invoiceId}, #{productId})")
    void insertInvoiceDetail(Integer invoiceId, Integer productId);


    //Select All
    @Select("""
            SELECT * FROM invoice;
            """)
    @Results(
            id = "invoiceMap",value = {
            @Result(property = "invoiceID", column = "invoice_id"),
            @Result(property = "invoiceDate", column = "invoice_date"),
            @Result(property = "customer", column = "customer_id",
                one = @One(select = "com.example.demomybatis.repository.CustomerRepository.getByID")
            ),
            @Result(property = "productList", column = "invoice_id",
                many = @Many(select = "com.example.demomybatis.repository.ProductRepository.getAllProductByInvoiceID")
            )
    }
    )
    List<Invoice> getAll();

    @Select("select * from invoice where invoice_id = #{id}")
    @ResultMap("invoiceMap")
    Invoice getInvoiceById(Integer id);

    @Select("""
            insert into invoice (invoice_date,customer_id) values(#{inR.invoiceDate},#{inR.customerID}) returning invoice_id
            """)
    Integer insertData(@Param("inR") InvoiceRequest invoiceRequest);





    @Delete("""
            Delete from invoice where invoice_id = #{id}
            """)
    @ResultMap("invoiceMap")
    Integer deleteByID(Integer id);

    @Delete("""
            Delete from invoice_detail where invoice_id = #{id}
            """)
    void deleteInvoiceDetail(Integer id);

    @Update("UPDATE invoice SET customer_id = #{iR.customerID} WHERE invoice_id = #{id}")
    @ResultMap("invoiceMap")
    Integer updateByID(Integer id, @Param("iR") InvoiceRequest invoiceRequest);
}
