package com.example.demomybatis.repository;

import com.example.demomybatis.model.entity.Customer;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
@Repository
public interface CustomerRepository {

    //Select All
    @Select("""
            SELECT * FROM customer order by customer_id asc;
            """)
    @Results(
            id = "customerMap",value = {
            @Result(property = "customerID", column = "customer_id"),
            @Result(property = "customerName", column = "customer_name"),
            @Result(property = "customerAdd", column = "customer_add"),
            @Result(property = "customerPh", column = "customer_ph"),
    }
    )
    List<Customer> getAll();

    //Select By ID
    @Select("""
            SELECT * FROM customer WHERE customer_id = #{id};
            """)
    @ResultMap("customerMap")
    Customer getByID(Integer id);

    //Insert Into Customer
    @Select("""
            INSERT into customer(customer_name, customer_add, customer_ph) VALUES (#{customer.customerName},#{customer.customerAdd}, #{customer.customerPh})
            RETURNING *
            """)
    @ResultMap("customerMap")
    Customer insertData(@Param("customer") Customer customer);


    //Update Data
    @Select("""
            UPDATE customer SET customer_name = #{customer.customerName} , customer_add = #{customer.customerAdd} , customer_ph = #{customer.customerPh}
            WHERE customer_id = #{id}
                RETURNING *
            """)
    @ResultMap("customerMap")
    Customer UpdateData(@PathVariable Integer id, @Param("customer") Customer customer);

    @Select("""
            DELETE from customer where customer_id = #{id}
            """)
    Customer deleteData(Integer id);
}
