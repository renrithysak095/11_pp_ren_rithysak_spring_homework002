package com.example.demomybatis;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(
        info = @Info(
                title = "Homework-002",
                version = "1.0",
                description = "Welcome to Spring API"
        )
)
public class DemoMyBatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoMyBatisApplication.class, args);
    }

}
